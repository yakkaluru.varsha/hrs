package com.hcl.util;

public class Query {

	// by admin
	public static String adminLogin="select * from admin where user_name=? and password=?";
	public static String adminViewDetails="select * from hoteldetails";
	public static String removeHotel="delete from hoteldetails where hotel_id=?";
	public static String addHotelDetails="insert into hoteldetails values(?,?,?,?,?,?)";
	public static String updateTotalRooms="Update hoteldetails set total_rooms=? where hotel_id=?";
	public static String UpdateTotalRoomsAndCost="Update hoteldetails set total_rooms=? ,cost_of_room=? where hotel_id=?";
	// by user
	public static String reserveHotel="insert into reservehotel values(?,?,?,?,?,?,?,?)";
	public static String editDepartureDate="Update reservehotel set departure_date=? where hotel_id=?";
	public static String editTotalRooms="Update reservehotel set total_rooms=? where hotel_id=?";
	public static String editDepartureAndArrivial="Update reservehotel set arrivail_date=?,departure_date=? where hotel_id=?";
	public static String cancelReservation="delete from reservehotel where hotel_id=?";
	
}
