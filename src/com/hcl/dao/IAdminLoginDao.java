package com.hcl.dao;
import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.HotelDetails;
import com.hcl.model.Reservation;

public interface IAdminLoginDao
{
    public int adminAuthentication(Admin admin);
    public List<HotelDetails> viewAllHotelDetails();
	public int removeHotelDetails(HotelDetails hotel);
	public int addHotelDetails(HotelDetails info);
	public int editTotalRooms(HotelDetails info);
	public int editTotalRoomsAndCost(HotelDetails info);
	
	
	public int reserveHotel(Reservation reservation);
	public int editDepartureDate(Reservation r);
	public int editTotalRooms1(Reservation r);
	public int editDepartureAndArrivial(Reservation r);
	public int cancelReservation(Reservation r);
	
}
