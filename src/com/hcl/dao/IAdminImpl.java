package com.hcl.dao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.hcl.model.Admin;
import com.hcl.model.HotelDetails;
import com.hcl.model.Reservation;
import com.hcl.util.Db;
import com.hcl.util.Query;

public class IAdminImpl implements IAdminLoginDao{
	PreparedStatement pst;
	ResultSet rs;
	int result;
	
	@Override
	public int adminAuthentication(Admin admin) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.adminLogin);
			pst.setString(1, admin.getUsername());
			pst.setString(2, admin.getPassword());
			rs=pst.executeQuery();
			while(rs.next())
			{
				result++;
				
			}
		}
		catch(ClassNotFoundException|SQLException e)
		{
			System.out.println("Exception occurs in admin Login");
			//e.printStackTrace();
   		}
		/*
		 * finally { try { pst.close(); rs.close(); Db.getConnection().close(); } catch
		 * (SQLException e) {
		 * 
		 * e.printStackTrace(); } catch (ClassNotFoundException e) {
		 * 
		 * e.printStackTrace(); }
		 * 
		 * }
		 */
		return result;
	}

	@Override
	public List<HotelDetails> viewAllHotelDetails() 
	{
		//result=0;
        List<HotelDetails> list=new ArrayList<HotelDetails>();
		try 
		{
			pst=Db.getConnection().prepareStatement(Query.adminViewDetails);
			rs=pst.executeQuery();
			while(rs.next())
			{
				HotelDetails info=new HotelDetails(rs.getInt(1),rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5), rs.getDouble(6));
				list.add(info);
			}
		} 
		catch (ClassNotFoundException | SQLException e) 
		{
			//e.printStackTrace();
			System.out.println("Exception occurs in View all hotel details");
		}
		
		return list;
	}

	@Override
	public int removeHotelDetails(HotelDetails hotel) {
		
		result=0;
		try {
				pst=Db.getConnection().prepareStatement(Query.removeHotel);
				pst.setInt(1, hotel.getHotelId());
				pst.executeUpdate();
			} catch (ClassNotFoundException|SQLException e) {
				
				e.printStackTrace();
			}
		return result;
			
		
	}

	@Override
	public int addHotelDetails(HotelDetails info) {
		result =0;
		try {
			
			pst=Db.getConnection().prepareStatement(Query.addHotelDetails);
			pst.setInt(1, info.getHotelId());
			pst.setString(2, info.getHotelName());
			pst.setString(3, info.getCity());
			pst.setString(4,info.getRoomType());
			pst.setInt(5,info.getTotalRooms());
			pst.setDouble(6, info.getRoomCost());
			result=pst.executeUpdate();
			
		}
		catch(ClassNotFoundException | SQLException e) 
		{
			System.out.println("Exception occurs in add hotels");
			
		}
		finally
		{
			try {
				Db.getConnection().close();
				pst.close();
			}
			catch(ClassNotFoundException | SQLException e)
			{
				
			}
		}
		return result;
	}

	@Override
	public int reserveHotel(Reservation reservation) {
		result=0;
		try {
			
			pst=Db.getConnection().prepareStatement(Query.reserveHotel);
			pst.setDate(1, (java.sql.Date) reservation.getArrivialDate());
			pst.setDate(2,  (java.sql.Date) reservation.getDepartureDate());
			pst.setInt(3,reservation.getNoOfDays());
			pst.setInt(4, reservation.getNoOfAdults());
			pst.setInt(5, reservation.getNoOfChildren());
			pst.setInt(6, reservation.getTotalRooms());
			pst.setString(7, reservation.getRoomType());
			pst.setInt(8, reservation.getHotelId());
			result=pst.executeUpdate();
			
		}
		catch(ClassNotFoundException | SQLException e) {
			//System.out.println("Exception occurs in reservation");
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int editTotalRooms(HotelDetails info) {
		result=0;
		try {
			
			pst=Db.getConnection().prepareStatement(Query.updateTotalRooms);
			pst.setInt(1, info.getTotalRooms());
			pst.setInt(2, info.getHotelId());
			result=pst.executeUpdate();
			
		}
		catch(ClassNotFoundException | SQLException e) {
			//System.out.println("Exception occurs in reservation");
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int editTotalRoomsAndCost(HotelDetails info) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.UpdateTotalRoomsAndCost);
			pst.setInt(1, info.getTotalRooms());
			pst.setDouble(2,info.getRoomCost());
			pst.setInt(3, info.getHotelId());
			result=pst.executeUpdate();
		}
		catch(ClassNotFoundException|SQLException e) {
			//System.out.println("Exception occurs in update");
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int editDepartureDate(Reservation r) {
		result=0;
		try {
			
			pst=Db.getConnection().prepareStatement(Query.editDepartureDate);
			pst.setInt(2, r.getHotelId());
			pst.setDate(1, r.getDepartureDate());
			result=pst.executeUpdate();
		}catch(ClassNotFoundException|SQLException e) {
			//System.out.println("Exception occurs in update");
			e.printStackTrace();
		}
		if(result==0) {
			System.out.println("done");
		}
		return result;
	}

	@Override
	public int editTotalRooms1(Reservation r) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.editTotalRooms);
			pst.setInt(2, r.getHotelId());
			pst.setInt(1, r.getTotalRooms());
			result=pst.executeUpdate();
		}
		catch(ClassNotFoundException|SQLException e) {
			//System.out.println("Exception occurs in update");
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int editDepartureAndArrivial(Reservation r) {
		result=0;
		try {
			pst=Db.getConnection().prepareStatement(Query.editDepartureAndArrivial);
			pst.setInt(3, r.getHotelId());
			pst.setDate(1,(java.sql.Date)r.getArrivialDate());
			pst.setDate(2, (java.sql.Date) r.getDepartureDate());
			result=pst.executeUpdate();
		}
		catch(ClassNotFoundException|SQLException e) {
			//System.out.println("Exception occurs in update");
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int cancelReservation(Reservation r) {
		try {
			pst=Db.getConnection().prepareStatement(Query.cancelReservation);
			pst.setInt(1, r.getHotelId());
			pst.executeUpdate();
		} catch (ClassNotFoundException|SQLException e) {
			
			e.printStackTrace();
		}
	return result;
	}

	
	

}
