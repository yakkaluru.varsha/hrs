package com.hcl.controller;

import com.hcl.dao.IAdminLoginDao;
import com.hcl.model.Admin;
import com.hcl.model.HotelDetails;
import com.hcl.model.Reservation;

import java.util.List;
import java.sql.Date;

import com.hcl.dao.IAdminImpl;

public class AdminController {
	int result;
	int result1;
	IAdminLoginDao dao = new IAdminImpl();

	public int adminAuthentication(String username, String password) {
		Admin admin = new Admin(username, password);
		return dao.adminAuthentication(admin);
	}

	public List<HotelDetails> viewAllHotelDetails() {
		return dao.viewAllHotelDetails();
	}

	public int removeHotelDetails(int hotelID) {

		HotelDetails hotelDetails = new HotelDetails();
		hotelDetails.setHotelId(hotelID);
		return dao.removeHotelDetails(hotelDetails);
	}

	public int addHotelDetails(int hotelId, String hotelName, String city, String roomType, int totalRooms,
			double cost) {
		HotelDetails info = new HotelDetails(hotelId, hotelName, city, roomType, totalRooms, cost);
		return dao.addHotelDetails(info);
	}

	public int editTotalRooms(int hid, int tr) {
		HotelDetails info = new HotelDetails();
		info.setHotelId(hid);
		info.setTotalRooms(tr);
		return dao.editTotalRooms(info);
	}

	public int editTotalRoomsAndCost(int hid1, int tr2, double cost) {
		HotelDetails info = new HotelDetails();
		info.setHotelId(hid1);
		info.setTotalRooms(tr2);
		info.setRoomCost(cost);
		return dao.editTotalRoomsAndCost(info);

	}

	public int reserveHotel(java.sql.Date ad, java.sql.Date dd, int noOfDays, int noOfAdults, int noOfChildren,
			int totalRooms, String roomType, int id) {
		
		Reservation info = new Reservation(ad, dd, noOfDays, noOfAdults,
				 noOfChildren, totalRooms, roomType,id);
				  
				  return dao.reserveHotel(info); 
	}

	public int editDepartureDate(int hotelId,Date dDate1) {
		Reservation r=new Reservation();
		r.setHotelId(hotelId);
		r.setDepartureDate(dDate1);
		return dao.editDepartureDate(r);
	}

	public int edittotalrooms1(int i, int rooms) {
		Reservation r=new Reservation();
		r.setHotelId(i);
		r.setTotalRooms(rooms);
		return dao.editTotalRooms1(r);
		
		
	}

	public int editDepartureDate(int h, Date aDateParse, Date dDateParse) {
		Reservation r=new Reservation();
		r.setHotelId(h);
		r.setArrivialDate(aDateParse);
		r.setDepartureDate(dDateParse);
		return dao.editDepartureAndArrivial(r);
		
	}

	public int cancelReservation(int hId) {
	Reservation r=new Reservation();
	r.setHotelId(hId);
	
		return dao.cancelReservation(r);
	}

	

	

}
