package com.hcl.model;

import java.sql.Date;

public class Reservation
{ 
	Date  arrivialDate;
	Date departureDate;
	int noOfDays;
	int noOfAdults;
	int noOfChildren;
	int totalRooms;
	String roomType;
	int hotelId;
	public Date getArrivialDate() {
		return arrivialDate;
	}
	public void setArrivialDate(Date arrivialDate) {
		this.arrivialDate = arrivialDate;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public int getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}
	public int getNoOfAdults() {
		return noOfAdults;
	}
	public void setNoOfAdults(int noOfAdults) {
		this.noOfAdults = noOfAdults;
	}
	public int getNoOfChildren() {
		return noOfChildren;
	}
	public void setNoOfChildren(int noOfChildren) {
		this.noOfChildren = noOfChildren;
	}
	public int getTotalRooms() {
		return totalRooms;
	}
	public void setTotalRooms(int totalRooms) {
		this.totalRooms = totalRooms;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	
	public int getHotelId() {
		return hotelId;
	}
	public void setHotelId(int hotelId) {
		this.hotelId = hotelId;
	}
	
	public Reservation(Date arrivialDate, Date departureDate, int noOfDays, int noOfAdults, int noOfChildren,
			int totalRooms, String roomType, int hotelId) {
		super();
		this.arrivialDate = arrivialDate;
		this.departureDate = departureDate;
		this.noOfDays = noOfDays;
		this.noOfAdults = noOfAdults;
		this.noOfChildren = noOfChildren;
		this.totalRooms = totalRooms;
		this.roomType = roomType;
		this.hotelId = hotelId;
	}
	public Reservation() {
		// TODO Auto-generated constructor stub
	}
	
	
	 

}
